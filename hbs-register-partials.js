var glob = require("glob");
//var hbs = require('hbs');
var fs = require('fs');
var path = require('path');

module.exports = (hbs, pathglob,options) => {
    console.log('\nregistering partials');
    if (!options) options = {};

    glob.sync(pathglob,options)
      .forEach(function (file) {
        var name = path.basename(file).replace(/\.hbs|_/g,'');                      console.log(' ','{{>'+name+'}}',file);
        hbs.registerPartial(name, fs.readFileSync(file, 'utf8'));
      });

  //console.log(hbs);
};